# proxy-app

NGINX Proxy App for our Project

## Usage

### Environment variable
Use this in gitlab CI/CD
* `LISTEN_PORT` - Port to listen to (default: `8000`)
* `APP_HOST` - Port to listen to (default: `app`)
* `APP_PORT` - Port to listen to (default: `9000`)